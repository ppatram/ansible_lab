# begin sendmail.mc
divert(-1)
#
#  SITA sendmail client configuration
#
divert(0)dnl

include(`/usr/share/sendmail-cf/m4/cf.m4')dnl
VERSIONID(`SITA linux sendmail client')dnl
OSTYPE(`linux')dnl

define(`confTIME_ZONE', `USE_TZ')dnl

define(`ALIAS_FILE',`/etc/aliases')dnl
define(`_USE_DECNET_SYNTAX_', `1')dnl support DECnet
define(`confDONT_INIT_GROUPS', `True')dnl
define(`confPID_FILE', `/var/run/sm-client.pid')dnl
dnl define(`confDIRECT_SUBMISSION_MODIFIERS',`C')dnl

FEATURE(`use_ct_file')dnl
FEATURE(`nouucp',`reject')dnl
FEATURE(`stickyhost')dnl

FEATURE(`genericstable', `hash -o /etc/mail/genericstable.db')dnl
FEATURE(local_procmail, `', `procmail -t -Y -a $h -d $u')dnl

FEATURE(`always_add_domain')dnl
FEATURE(`masquerade_entire_domain')dnl
FEATURE(`masquerade_envelope')dnl
FEATURE(`allmasquerade')dnl
MASQUERADE_AS(`sita.aero')dnl
MASQUERADE_DOMAIN(`sita.aero')dnl

EXPOSED_USER(`root')dnl
LOCAL_USER(`root')dnl

dnl define(`confMAX_MESSAGE_SIZE',`10000000')dnl
dnl define(`confDONT_INIT_GROUPS', `True')dnl
dnl
define(`confDEF_USER_ID',``8:12'')dnl
define(`confDONT_PROBE_INTERFACES',true)dnl

define(`confTO_QUEUEWARN_DSN',`')dnl
define(`confTO_QUEUERETURN_DSN',`12h')dnl

define(`PROCMAIL_MAILER_PATH', `/usr/bin/procmail')dnl
define(`SMART_HOST',`{{ MAIL_GATEWAY }}')dnl

# this prevents the need for setting sendmail binary setuid
define(`QUEUE_DIR',`/var/tmp')dnl

# this prevents bounces from filling up the filesystem
define(`confDEAD_LETTER_DROP',`/dev/null')dnl

dnl FEATURE(`msp', `SITA_MAIL_GATEWAY')dnl

MAILER(procmail)dnl
MAILER(smtp)dnl
# end sendmail.mc
