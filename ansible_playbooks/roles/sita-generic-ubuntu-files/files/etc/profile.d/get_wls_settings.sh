#!/bin/bash
# parmanand.patram@sita.aero; 2019-11-08
# print out the WLS environment settings on login
#
basedir="/app/services/mw"
cd $basedir 2>/dev/null
filename="config.xml"

for i in `find . -path "*/config/*" -type f -name $filename 2>/dev/null`; do
   envname=`echo $i | cut -d/ -f2`
   dirname=`dirname $i`
   cd $dirname 2>/dev/null

   # osb?
   if [ `grep -ic osb $filename 2>/dev/null` == 0 ]; then
      admhost=`grep -A6 '<name>AdminServer</name>' $filename 2>/dev/null | grep 'listen-address' | tail -1 | cut -d\> -f2 | cut -d\< -f1`
      admport=`grep -A6 '<name>AdminServer</name>' $filename 2>/dev/null | grep 'listen-port' | tail -1 | cut -d\> -f2 | cut -d\< -f1`
      nmport=`grep -A2 '<nm-type>' $filename 2>/dev/null | grep 'listen-port' | head -1 | cut -d\> -f2 | cut -d\< -f1`
      msport=`grep -A2 '<machine>' $filename 2>/dev/null | tr '\n' '*' | sed -- 's/\*//g; s/--/\n/g' | egrep -v '<machine>\s*<name>' | sed 's/  <machine>/- machine:/; s|</machine>|\n|; s/<listen-port>/listen-port: /; s|</listen-port>|\n|; s|<cluster>|cluster: |; s|</cluster>||'`
   else
      admhost=`grep -A2 '<machine>' $filename 2>/dev/null | tr '\n' ' ' | sed -- 's/--/\n/g' | egrep -v '<machine>\s*<name>' | head -1 | tr '>' '\n' | egrep -v '^\s' | cut -d'<' -f1 | head -1`
      admport=`grep -A2 '<machine>' $filename 2>/dev/null | tr '\n' ' ' | sed -- 's/--/\n/g' | egrep -v '<machine>\s*<name>' | head -1 | tr '>' '\n' | egrep -v '^\s' | cut -d'<' -f1 | tail -1`
      nmport=`grep -A2 '<nm-type>' $filename 2>/dev/null | grep 'listen-port' | head -1 | cut -d\> -f2 | cut -d\< -f1`
      msport=`grep -A2 '<machine>' $filename 2>/dev/null | tr '\n' ' ' | sed -- 's/--/\n/g' | egrep -v '<machine>\s*<name>' | sed '1d' | sed -- 's/  <machine>/- machine:/; s|</machine>|\n|; s/<listen-port>/listen-port: /; s|</listen-port>|\n|; s|<cluster>|cluster: |; s|</cluster>||'`
   fi

   echo -e "\n\n\n#------------------- Domain: $envname --------------------------#"
   echo "$envname:"
   echo "  AdminServer:"
   echo "    machine: $admhost"
   echo "    listen-port: $admport"
   echo
   echo "NodeManager:"
   echo "  listen-port: $nmport"
   echo
   echo "ManagedServers:"
   echo "$msport" | sed 's/^/  /'
   echo "#-------------------------------------------------------#"
   cd $basedir

done
echo -e "\n\n\n"
cd ~
