# @version $Id: chksys.sh 2130 2017-06-02 15:09:47Z parmanand.patram@sita.aero $
if [ "$USER" == "root" ]
then
	VMWARE=`lspci | grep -i vmware`
	if [ "$VMWARE" != "" ] && [ ! -d /etc/vmware-tools ]
	then
		echo
		echo "This virtual machine does not have VMware Tools installed!"
		echo
		echo "Please install VMware Tools"
		echo 
	fi
fi
