# @version $Id: history.sh 2130 2017-06-02 15:09:47Z parmanand.patram@sita.aero $


HISTSIZE=32000
HISTFILESIZE=32000
HISTTIMEFORMAT="%F %T "

# this from redhat article here:
# https://access.redhat.com/solutions/20707
function history_to_syslog
{
	# Preserve setting of IFS including whether it is set at all and then unset it
	if [ -n "${IFS+1}" ]; then preserve_IFS_state=${IFS}; else unset preserve_IFS_state; fi;
        unset IFS

	TTY=`tty 2>/dev/null | grep -v 'not a tty'`
	PERSON=`ls -ld $TTY | awk '{print $3}'`
	#PORT=`who am i | awk '{ print $5 }' | sed 's/(//g' | sed 's/)//g'`
	declare cmd
	declare p_dir
	declare LOG_NAME
	cmd=$(history 1)
	#cmd=$(echo $cmd |awk '{print substr($0,length($1)+2)}')
	# strip off the timestamp from the command
	cmd=$(echo $cmd | awk '{print substr($0,length($1)+length($2)+length($3)+4)}' )
	p_dir=$(pwd)
	LOG_NAME=$(echo $LOGNAME)
	SERIAL=`date +%s`
	if [ "$cmd" != "$old_command" ]; then
		logger -p user.warning -t "logger[${SERIAL}]" "TTY=$TTY ; PWD=$p_dir ; USER=$PERSON as $LOG_NAME ; COMMAND='${cmd}'"
	fi
	old_command=$cmd

	# Restore state and setting of IFS environment variable
	if [ -n "${preserve_IFS_state+1}" ]; then IFS=${preserve_IFS_state}; fi
}

# only log if runlevel is 3 -- keeps from busting up the rescue environment.
if [ "`/sbin/runlevel 2>/dev/null`" == "N 3" ]; then
	trap history_to_syslog DEBUG || EXIT
fi

