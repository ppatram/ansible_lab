# @version $Id: alias.sh 2130 2017-06-02 15:09:47Z parmanand.patram@sita.aero $
alias cl='cd;clear'
alias h='history'
alias l='ls -lFhart'
alias l.='ls -d .* --color=tty'
alias ll='ls -l --color=tty'
alias ls='ls --color=tty'
alias root='sudo su'
alias s='source ~/.bashrc'
