# @version $Id: prompt.sh 2130 2017-06-02 15:09:47Z parmanand.patram@sita.aero $
if [ "$TERM" == "xterm" ] || [ "$TERM" == "ansi" ] || [ "$TERM" == "linux" ]; then
   if [ "$USER" == "root" ]; then
      # Prompt color based on dev/qa/prod
      HNAME=`hostname -s`
      ENVIRON=${HNAME: -1}
      if [ "$ENVIRON" == 'd' ]; then
	 export PS1="\[$(tput setaf 2;tput bold)\][\u@\h \w]# \[$(tput sgr0)\]"
      elif [ "$ENVIRON" == 'q' ]; then
	 export PS1="\[$(tput setaf 3;tput bold)\][\u@\h \w]# \[$(tput sgr0)\]"
      elif [ "$ENVIRON" == 'p' ]; then
	 export PS1="\[$(tput setaf 5;tput bold)\][\u@\h \w]# \[$(tput sgr0)\]"
      else
	 export PS1="\[$(tput setaf 5;tput bold)\][\u@\h \w]# \[$(tput sgr0)\]"
      fi
   else
      # all other users get default prompt color
      export PS1="\[$(tput setaf 6)\][\u@\h \w]$ \[$(tput sgr0)\]"
   fi
fi

