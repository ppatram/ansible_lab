To create more than one lv on each vg:

    xsgrpv_vmdk_storage:
      ############################################ device and vgname the same
    - device: /dev/sdc
      vgname: ppfs_ext3fs_1gig_parmtest
      ###################################### unique mount/size/lvname
      mountpoint: /ppfs/ext3fs_on_same_disk
      lvsize: 1024m
      lvol_name: first

      ############################################ device and vgname the same
    - device: /dev/sdc
      vgname: ppfs_ext3fs_1gig_parmtest
      ###################################### unique mount/size/lvname
      mountpoint: /ppfs/ext3fs_1gig
      lvsize: 2048m
      lvol_name: second
      fstype: ext3  #### even able to use different fstypes




To resize a existing vg/lv
      ################################# simply put the new disk name you wish to add
    - device: /dev/sdd,/dev/sde
      mountpoint: /ppfs/multi-disks



Disk encryption settings

The following vars must be set:

    xsgrpv_tang_nbde:
    - url: 'http://ldatsxs103q.atl2.dc.sita.aero'
      cert:  '-z_iyn1CfeLW0dvjZfCEX2mak84'
    - url: 'http://ldatsxs102q.atl2.dc.sita.aero'
      cert:  'Zqk3F7AY-4o6ymKARhmEsiG_ME0'

    xsgrpv_vmdk_storage:
    - device: /dev/sdc
      owner: mongod
      group: mongod
      mode: "0755"
      mountpoint: /mongodb/data
      mountopts: noatime,nodiratime
  --->nbde: true

