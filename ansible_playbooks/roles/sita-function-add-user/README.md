

sita_user can be either a user from the defined users (defaults/main.yml)
example:
sita_user: oracle
or
username:uid 
to specify an arbitrary uid
example:
sita_user: bluett:5555



group names can be a mix of pre-defined groupnames and gname:gid elements.
example:
sita_groups: group1:9999,group2:9998,group3:9997,oinstall,vgrftpadmin

For a specific example see tests/test.yml
