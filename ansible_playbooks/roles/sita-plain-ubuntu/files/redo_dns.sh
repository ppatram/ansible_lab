#!/bin/bash -x

fqdn=$1
newip=$2

server=`echo $fqdn | cut -d. -f1`
scnfqdn="$(echo $fqdn | grep -e '-scn')"
cat /root/.ssh/.kinit | kinit admin

zone=`echo $fqdn | cut -d. -f2-`
# does forward zone exist?
ipa dnszone-find $zone >/dev/null 2>&1
retval=$?
if [ $retval -eq 0 ]; then
        echo "++DNS zone '$zone' OK"
else
        echo "--DNS zone '$zone' does not exist"
        echo +++++++ ipa dnszone-add  $zone
        ipa dnszone-add  $zone  >/dev/null 2>&1
fi

echo "ipa dnsrecord-find $zone $server | grep 'A record:' | cut -d: -f2 | sed 's/ *//g'"
ip=`ipa dnsrecord-find $zone $server | grep 'A record:' | cut -d: -f2 | sed 's/ *//g'`

if [ "$scnfqdn" ]; then
        echo "--DNS A record needs changing"
        echo +++++++ ipa dnsrecord-del $zone $server --del-all
        ipa dnsrecord-del $zone $server --del-all >/dev/null 2>&1

	newscnips="$( echo $newip | sed 's/,/ /g')"
	for scnips in $newscnips; do
		echo +++++++ ipa dnsrecord-add $zone $server --a-rec $scnips
		ipa dnsrecord-add $zone $server --a-rec $scnips  >/dev/null 2>&1
	done
elif [ "$ip" == "$newip" ]; then
        echo "++DNS A record is OK"
else
        echo "--DNS A record needs changing"
        echo +++++++ ipa dnsrecord-del $zone $server --del-all
        ipa dnsrecord-del $zone $server --del-all >/dev/null 2>&1
        echo +++++++ ipa dnsrecord-add $zone $server --a-rec $newip
        ipa dnsrecord-add $zone $server --a-rec $newip  >/dev/null 2>&1
fi

if [ "$scnfqdn" ]; then
	ascnip="$(echo $newip | cut -d',' -f1)"
	revzone=`host $ascnip | sed 's/^Host //' | awk '{print $1}' | cut -d. -f2-`
	echo "revzone: $revzone"
	revhost=`host $ascnip | sed 's/^Host //' | awk '{print $1}' | cut -d. -f1`
	echo "revhost: $revhost"
	firstip=`echo $ascnip | sed -E 's/\.[0-9]*$/.1/'`
	echo "firstip: $firstip"
else
	revzone=`host $newip | sed 's/^Host //' | awk '{print $1}' | cut -d. -f2-`
	revhost=`host $newip | sed 's/^Host //' | awk '{print $1}' | cut -d. -f1`
	firstip=`echo $newip | sed -E 's/\.[0-9]*$/.1/'`
fi


# does reverse zone exist?
ipa dnszone-find $revzone >/dev/null 2>&1
retval=$?
if [ $retval -eq 0 ]; then
        echo "++DNS PTR zone '$revzone' OK"
else
        echo "--DNS PTR zone '$revzone' does not exist"
        echo +++++++ ipa dnszone-add  $revzone --name-from-ip=${firstip}
        ipa dnszone-add  $revzone --name-from-ip=${firstip}  >/dev/null 2>&1
fi

# does the reverse record match?
if [ "$scnfqdn" ]; then
	echo "Scan PTR IP's: $newip"
	newscnips="$( echo $newip | sed 's/,/ /g')"
	for scnptrip in $newscnips; do
		revhost=`host $scnptrip | sed 's/^Host //' | awk '{print $1}' | cut -d. -f1`
	        echo "--DNS PTR record incorrect"
        	echo +++++++ ipa dnsrecord-del $revzone $revhost --del-all
        	ipa dnsrecord-del $revzone $revhost --del-all  >/dev/null 2>&1

		echo +++++++ ipa dnsrecord-add $revzone $revhost --ptr-rec=${fqdn}.
		ipa dnsrecord-add $revzone $revhost --ptr-rec=${fqdn}.  >/dev/null 2>&1
	done
elif [ "`host $newip | awk '{print $5}'`" == "${fqdn}." ]; then
        echo "++DNS PTR record OK"
else
        echo "--DNS PTR record incorrect"
        echo +++++++ ipa dnsrecord-del $revzone $revhost --del-all
        ipa dnsrecord-del $revzone $revhost --del-all  >/dev/null 2>&1
        echo +++++++ ipa dnsrecord-add $revzone $revhost --ptr-rec=${fqdn}.
        ipa dnsrecord-add $revzone $revhost --ptr-rec=${fqdn}.  >/dev/null 2>&1
fi

