#!/bin/bash
# @version $Id: sshkeys.sh 1121 2015-09-08 20:53:24Z parmanand.patram $ 
#
# 2015-09-04    - david.ausman@sita.aero
#               - built this script to manage key installation for weblogic, osb, and soa servers 
# 2015-09-08    - parmanand.patram@sita.aero
#		- Updated script to integrate with the piece in ITPAM which generates and
#		    uploads the priv/pub key-pairs to the server.
# 2015-10-30	- david.ausman@sita.aero
#		- updated the script so it could be run multiple times without duplicating keys
#		- added the check for FULL_BUILD.SPECS file in multiple locations
#
# 2016-06-21	- david.ausman@sita.aero
#		- added check for WLS_GEN to process Generic WLS clusters and install keys

# 2019-05-02	- parmanand.patram@sita.aero
#		- updated for ansible/vra to use cli inputs instead of files
#
set -euo pipefail
TMPFILE=`mktemp /tmp/ssh_massage.XXXXXX`

SSHFROM=`echo $@ | awk -F'sshfrom=' '{print $2}' | awk '{print $1}' | sed 's/,/ /g'`
LOCALHOME=`echo $@ | awk -F'homebase=' '{print $2}' | awk '{print $1}'`
APPTYPE=`echo $@ | awk -F'mtype=' '{print $2}' | awk '{print $1}'`
SRVRTYPE="$(echo $APPTYPE | cut -d '_' -f1 )"
SRVRFLAVOR="$(echo $APPTYPE | cut -d '_' -f2)"
MYHNAME="$(hostname -s)"

SSHFROM_HOST_LIST=""
SSHFROM_CLTYPE_LIST=""
WLS_ADMINHOST=""
OSBSOA_ADMINHOST=""

# get WLS admin host and osbsoa admin host
for ENT in $SSHFROM; do
	HOST=`echo $ENT | cut -d: -f1`
	CLTYPE=`echo $ENT | cut -d: -f2`
	# store the values to the respective arrays
	SSHFROM_HOST_LIST="${SSHFROM_HOST_LIST} ${HOST}"
	SSHFROM_CLTYPE_LIST="${SSHFROM_CLTYPE_LIST} ${CLTYPE}"
	if [ "$CLTYPE" == 'WLS' ]; then
		WLS_ADMINHOST=$HOST
	elif [ "$CLTYPE" == 'OSBSOA' ]; then
		OSBSOA_ADMINHOST=$HOST
	fi
done

# pull out my MTYPE
MTYPE=`echo $SSHFROM | tr ' ' '\n' | grep "^${MYHNAME}:" | cut -d: -f2`

# parmanand.patram@sita.aero; 2015-09-08 -- grab a few other variables

#CLUSTERROOT="/root/.ssh/clusterroot"
CLUSTERROOT="/root/.ssh"

# parmanand.patram@sita.aero; 2015-09-08 -- change paths to match what is being laid down
PUBKEY="authorized_keys"
WLSADMIN_ORACLE="${CLUSTERROOT}/${WLS_ADMINHOST}/oracle/.ssh"
WLSADMIN_WEBLOGIC="${CLUSTERROOT}/${WLS_ADMINHOST}/weblogic/.ssh"

OSBSOAADMIN_ORACLE="${CLUSTERROOT}/${OSBSOA_ADMINHOST}/oracle/.ssh"
OSBSOAADMIN_OSB="${CLUSTERROOT}/${OSBSOA_ADMINHOST}/osb/.ssh"
OSBSOAADMIN_SOA="${CLUSTERROOT}/${OSBSOA_ADMINHOST}/soa/.ssh"
# parmanand.patram@sita.aero; 2015-09-08 -- change paths to match what is being laid down


AUTHKEYS="authorized_keys"
SSHROOT_ORACLE="${LOCALHOME}/oracle/.ssh"
SSHROOT_WEBLOGIC="${LOCALHOME}/weblogic/.ssh"
SSHROOT_OSB="${LOCALHOME}/osb/.ssh"
SSHROOT_SOA="${LOCALHOME}/soa/.ssh"


restrict_permissions() {
        chown -R ${1}:${2} ${LOCALHOME}/${1}/.ssh
        chown ${1}:root ${LOCALHOME}/${1}/.ssh
        chmod 700 ${LOCALHOME}/${1}/.ssh
        chown root:root ${LOCALHOME}/${1}/.ssh/authorized_keys
        chmod 444 ${LOCALHOME}/${1}/.ssh/authorized_keys

	if [ -f ${LOCALHOME}/${1}/.ssh/id_rsa ]; then
		chown ${1}:root ${LOCALHOME}/${1}/.ssh/id_rsa
		chmod 600 ${LOCALHOME}/${1}/.ssh/id_rsa
	fi

	if [ -f ${LOCALHOME}/${1}/.ssh/id_rsa.pub ]; then
		chown ${1}:root ${LOCALHOME}/${1}/.ssh/id_rsa.pub
		chmod 644 ${LOCALHOME}/${1}/.ssh/id_rsa.pub
	fi
}


# ===== MAIN =====
#

if [ "$SRVRTYPE" == "WLS" ]; then
	# check if this is a Horizon weblogic admin server
	if [[ "$APPTYPE" =~ "WLS_HZN_ADM_WLS" ]]; then
		mkdir -p $SSHROOT_ORACLE
		mkdir -p $SSHROOT_WEBLOGIC

		# delete existing authorized_keys file and copy new keys
        	cat ${WLSADMIN_ORACLE}/${PUBKEY} > ${SSHROOT_ORACLE}/${AUTHKEYS}
        	cat ${WLSADMIN_ORACLE}/${PUBKEY} > ${SSHROOT_WEBLOGIC}/${AUTHKEYS}
		# append the remaining keys
        	cat ${WLSADMIN_WEBLOGIC}/${PUBKEY} >> ${SSHROOT_WEBLOGIC}/${AUTHKEYS}
        	cat ${OSBSOAADMIN_ORACLE}/${PUBKEY} >> ${SSHROOT_ORACLE}/${AUTHKEYS}
        	cat ${OSBSOAADMIN_ORACLE}/${PUBKEY} >> ${SSHROOT_WEBLOGIC}/${AUTHKEYS}
		cat ${TOP}/sita_vmconfig/bin/STATIC/OEM_WLS_KEY.${ENV} >> ${SSHROOT_ORACLE}/${AUTHKEYS}

		# delete existing id_rsa* files
		rm -f ${SSHROOT_ORACLE}/id_rsa*
		rm -f ${SSHROOT_WEBLOGIC}/id_rsa*
		# copy new id_rsa* files
		/bin/cp -f ${WLSADMIN_ORACLE}/id_rsa* $SSHROOT_ORACLE
		/bin/cp -f ${WLSADMIN_WEBLOGIC}/id_rsa* $SSHROOT_WEBLOGIC
	
		restrict_permissions oracle voyager
		restrict_permissions weblogic voyager

	# check if this is a Horizon weblogic OSB/SOA admin server
	elif [[ "$APPTYPE" =~ "WLS_HZN_ADM_OSBSOA" ]]; then
		mkdir -p $SSHROOT_ORACLE
		mkdir -p $SSHROOT_OSB
		mkdir -p $SSHROOT_SOA

		# delete existing authorized_keys file and copy new keys
        	cat ${OSBSOAADMIN_ORACLE}/${PUBKEY} > ${SSHROOT_ORACLE}/${AUTHKEYS}
		cat ${OSBSOAADMIN_ORACLE}/${PUBKEY} > ${SSHROOT_OSB}/${AUTHKEYS}
 		cat ${OSBSOAADMIN_ORACLE}/${PUBKEY} > ${SSHROOT_SOA}/${AUTHKEYS}
		# append the remaining keys
        	cat ${WLSADMIN_ORACLE}/${PUBKEY} >> ${SSHROOT_ORACLE}/${AUTHKEYS}
        	cat ${OSBSOAADMIN_OSB}/${PUBKEY} >> ${SSHROOT_OSB}/${AUTHKEYS}
       		cat ${OSBSOAADMIN_SOA}/${PUBKEY} >> ${SSHROOT_SOA}/${AUTHKEYS}
		cat ${TOP}/sita_vmconfig/bin/STATIC/OEM_WLS_KEY.${ENV} >> ${SSHROOT_ORACLE}/${AUTHKEYS}

		# delete existing id_rsa* files
		rm -f ${SSHROOT_ORACLE}/id_rsa*
		rm -f ${SSHROOT_OSB}/id_rsa*
		rm -f ${SSHROOT_SOA}/id_rsa*
		# copy new id_rsa* files
		/bin/cp -f ${OSBSOAADMIN_ORACLE}/id_rsa* $SSHROOT_ORACLE
		/bin/cp -f ${OSBSOAADMIN_OSB}/id_rsa* $SSHROOT_OSB
		/bin/cp -f ${OSBSOAADMIN_SOA}/id_rsa* $SSHROOT_SOA

		restrict_permissions oracle voyager
		restrict_permissions osb voyager
		restrict_permissions soa voyager

	# check if this is a Horizon OSB or SOA server
	elif [[ "$APPTYPE" =~ "WLS_HZN_HO" ]]; then
		# if a SOA server, create soa keys, otherwise, create osb keys
		if [[ "$APPTYPE" =~ "WLS_HZN_HOS" ]]; then
			mkdir -p $SSHROOT_ORACLE
			mkdir -p $SSHROOT_SOA

			# delete existing authorized_keys file and copy new keys
        		cat ${OSBSOAADMIN_ORACLE}/${PUBKEY} > ${SSHROOT_ORACLE}/${AUTHKEYS}
        		cat ${OSBSOAADMIN_ORACLE}/${PUBKEY} > ${SSHROOT_SOA}/${AUTHKEYS}
			# append the remaining keys
        		cat ${OSBSOAADMIN_SOA}/${PUBKEY} >> ${SSHROOT_SOA}/${AUTHKEYS}
			cat ${TOP}/sita_vmconfig/bin/STATIC/OEM_WLS_KEY.${ENV} >> ${SSHROOT_ORACLE}/${AUTHKEYS}

			restrict_permissions oracle voyager
			restrict_permissions soa voyager
		else
			mkdir -p $SSHROOT_ORACLE
			mkdir -p $SSHROOT_OSB

			# delete existing authorized_keys file and copy new keys
        		cat ${OSBSOAADMIN_ORACLE}/${PUBKEY} > ${SSHROOT_ORACLE}/${AUTHKEYS}
        		cat ${OSBSOAADMIN_ORACLE}/${PUBKEY} > ${SSHROOT_OSB}/${AUTHKEYS}
			# append the remaining keys
        		cat ${OSBSOAADMIN_OSB}/${PUBKEY} >> ${SSHROOT_OSB}/${AUTHKEYS}
			cat ${TOP}/sita_vmconfig/bin/STATIC/OEM_WLS_KEY.${ENV} >> ${SSHROOT_ORACLE}/${AUTHKEYS}

			restrict_permissions oracle voyager
			restrict_permissions osb voyager
		fi
	# check if this is a Horizon weblogic server
	elif [[ "$APPTYPE" =~ "WLS_HZN_HW" ]]; then
		mkdir -p $SSHROOT_ORACLE
		mkdir -p $SSHROOT_WEBLOGIC

		# delete existing authorized_keys file and copy new keys
        	cat ${WLSADMIN_ORACLE}/${PUBKEY} > ${SSHROOT_ORACLE}/${AUTHKEYS}
        	cat ${WLSADMIN_ORACLE}/${PUBKEY} > ${SSHROOT_WEBLOGIC}/${AUTHKEYS}
		# append the remaining keys
        	cat ${WLSADMIN_WEBLOGIC}/${PUBKEY} >> ${SSHROOT_WEBLOGIC}/${AUTHKEYS}
        	cat ${OSBSOAADMIN_ORACLE}/${PUBKEY} >> ${SSHROOT_ORACLE}/${AUTHKEYS}
        	cat ${OSBSOAADMIN_ORACLE}/${PUBKEY} >> ${SSHROOT_WEBLOGIC}/${AUTHKEYS}
		cat ${TOP}/sita_vmconfig/bin/STATIC/OEM_WLS_KEY.${ENV} >> ${SSHROOT_ORACLE}/${AUTHKEYS}

		restrict_permissions oracle voyager
		restrict_permissions weblogic voyager

	# check if this is for a Generic WLS cluster
	elif [[ "$APPTYPE" =~ "WLS_GEN" ]]; then
		mkdir -p $SSHROOT_ORACLE
		mkdir -p $SSHROOT_WEBLOGIC

		# delete existing authorized_keys file and copy new keys
        	cat ${WLSADMIN_ORACLE}/${PUBKEY} > ${SSHROOT_ORACLE}/${AUTHKEYS}
        	#cat ${WLSADMIN_ORACLE}/${PUBKEY} > ${SSHROOT_WEBLOGIC}/${AUTHKEYS}
		# append the remaining keys
        	cat ${WLSADMIN_WEBLOGIC}/${PUBKEY} >> ${SSHROOT_WEBLOGIC}/${AUTHKEYS}
		cat ${TOP}/sita_vmconfig/bin/STATIC/OEM_WLS_KEY.${ENV} >> ${SSHROOT_ORACLE}/${AUTHKEYS}

		# delete existing id_rsa* files
		rm -f ${SSHROOT_ORACLE}/id_rsa*
		rm -f ${SSHROOT_WEBLOGIC}/id_rsa*
		# copy new id_rsa* files
		/bin/cp -f ${WLSADMIN_ORACLE}/id_rsa* $SSHROOT_ORACLE
		/bin/cp -f ${WLSADMIN_WEBLOGIC}/id_rsa* $SSHROOT_WEBLOGIC
	
		restrict_permissions oracle 
		restrict_permissions weblogic 

	else
		echo "Not adding keys"		
	fi
	

elif [ "$MTYPE" == "RAC" ]; then
	# we will assume that the cltypes for RAC will all be 'RAC'.. to be 
	# fixed later with a rewrite of this script to use a mapping
	# table
	for SSHFROM_HOST in $SSHFROM_HOST_LIST; do
		SRCDIR="${CLUSTERROOT}/${SSHFROM_HOST}/oracle/.ssh"
		DESTDIR="${LOCALHOME}/oracle/.ssh"
		mkdir -p ${DESTDIR}
		if [ "$SSHFROM_HOST" == "$MYHNAME" ]; then
			# copy the private/public key
			/bin/cp -f ${SRCDIR}/id_rsa* ${DESTDIR}/
		fi
		# append the authorized_keys
		/bin/cat  ${SRCDIR}/authorized_keys ${DESTDIR}/authorized_keys | sort | uniq > $TMPFILE
		/bin/cp -f $TMPFILE ${DESTDIR}/authorized_keys
		restrict_permissions oracle dba
	done
else
	echo "No processing required. Skipping $0"
	rm -f $TMPFILE
	exit 99
fi

rm -f $TMPFILE

