#!/bin/bash -x
# @version $Id: generate_keys.sh 1177 2015-10-07 16:59:28Z parmanand.patram@sita.aero $
# Filepath: ~/PyCharmProjects/dc_itpam/post/vmconfig_rhel72/itpam_scriptsdepot
# parmanand.patram@sita.aero; 2015-09-08
# This script will generated pub/priv key pairs for various users
# based on the type of cluster which is requested
# 2017-04-13 -- parmanand.patram@sita.aero
#		-- use host fqdn and nfs fqdn to allow transfer over big pipe for WLS builds.

USAGE="
Usage: 
$0 <shKeyFrom> <domainName>
where 
. sshKeyFrom is a comma separated list of  node:clusterType <node1:clusterType,node2:clusterType,..,nodeN:clusterType> 
  from which a user can initiate ssh keyless login.
  clusterType is currently one of <WLS|OSBSOA|RAC>

. domainName is the domain name of the environment where 
  the servers are being deployed.

ex:
$0 lahznyy100d:WLS,lahznyy101d:OSBSOA atl2.dc.sita.aero

"

SSHKEYFROM=${1:-ERROR}
DOMNAME=${2:-ERROR}
IPLIST=${3:-ERROR}

ULIST_RAC='oracle'
ULIST_WLS='oracle weblogic'
ULIST_OSBSOA='oracle weblogic osb soa'

BASEDIR='/tower_shared/ssh-keys'
KEYTYPE='rsa'

# make sure all params given
if [ "`echo $SSHKEYFROM $DOMAINNAME | grep ERROR`" ]; then
	echo "$USAGE"; exit 1
fi

genkeys(){
	THISHOST=$1
	CLUTYPE=$2
	eval "USERLIST=\$ULIST_${CLUTYPE}"

	if [ "$USERLIST" ]; then
		for USERNAME in $USERLIST; do
			KEYDIR="${WORKDIR}/${THISHOST}/${USERNAME}/.ssh"
			KEYFILE="${KEYDIR}/id_${KEYTYPE}"
			PUBFILE="${KEYDIR}/id_${KEYTYPE}.pub"
			AUTHFILE="${KEYDIR}/authorized_keys"
			if [ $IPLIST != 'ERROR' ]; then
				FROM_STR="from=\"127.0.0.1,$IPLIST\""
			else
				IP=`host ${THISHOST}.${DOMNAME} | awk '{print $4}'`
				NFSIP=`host ${THISHOST}-nfs.${DOMNAME} | awk '{print $4}'`
				FROM_STR="from=\"127.0.0.1,$IP,$NFSIP\""
			fi
			THIS_FQDN="${THISHOST}.${DOMNAME}"
			THIS_NFS_FQDN="${THISHOST}-nfs.${DOMNAME}"
			if [ "$THIS_FQDN" ]; then
				mkdir -p $KEYDIR
				chown root:root $KEYDIR
				if [ ! -f "$KEYFILE" ]; then
					yes no | ssh-keygen -t${KEYTYPE}  -f $KEYFILE -N "" -C "${USERNAME}@${THISHOST}.${DOMNAME}"
					#echo -n "from=\"127.0.0.1,$THIS_FQDN,$THIS_NFS_FQDN\" " > $AUTHFILE
					#echo -n "$FROM_STR " > $AUTHFILE
					cat $PUBFILE >> $AUTHFILE
					chmod -R 700 ${WORKDIR}/${THISHOST}
				else
					echo "Keyfile '$KEYFILE' already exists. Skipping!"
				fi
			fi
		done
	else
			echo "Unable to get userlist. Unknown cluster type '$CLUTYPE'? Aborting!"
			exit 6
	fi
}



WORKDIR=${BASEDIR}
mkdir -p ${WORKDIR}
cd $WORKDIR
TMP=`echo $SSHKEYFROM | sed 's/,/ /g'`
for I in $TMP; do
	H=`echo $I | cut -d: -f1`	
	C=`echo $I | cut -d: -f2`	
	if [ "$H" ] && [ "$C" ]; then
		genkeys $H $C
	else
		echo "SSHKEYFROM is malformatted. Cannot get host:cluster format from every entity. Aborting!"
		exit 4
	fi
done

