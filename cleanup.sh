#!/bin/bash -x

#unmount and remove all mounted fs
fslist=$(grep ^/dev/mapper /etc/fstab | awk '{print $2}')
for fs in $fslist; do
  # unmount fs
  umount $fs
  # undo fstab
  sed -E -i "/^\/dev\/mapper\//d" /etc/fstab
done

lvs=$(lvscan | awk '{print $2}' | sed 's/^.//; s/.$//')

for lv in $lvs; do
  # remove all lvs
  lvremove -f -f $lv
done

vgs=$(vgs | awk '{print $1}'  | grep -v ^VG$)
for vg in $vgs; do
  # remove all vgs
  vgchange -an $vg
  vgremove -f -f $vg
done

pvs=$(pvs | awk '{print $1}'  | grep -v ^PV$)
for pv in $pvs; do
  # remove all pvs
  pvremove -f -f $pv

  # wipe disk label
  disk=$(echo $pv | sed 's/1$//')
  dd if=/dev/zero of=$disk bs=50M count=1
done
