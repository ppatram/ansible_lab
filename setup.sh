#!/bin/bash
## ppatram@gmail.com; setup lab system (ubuntu 16.04 Azure)
## run this as root on the 'ansible' server

# add user 'generic'
useradd -s /bin/generic_login -m -d /home/generic generic
echo -e 'generic\ngeneric' | passwd generic

# sudo rules 
egrep '^generic\s' /etc/sudoers || echo 'generic\t' >> /etc/sudoers
sed -Ei 's/^generic\s*.*$/generic\tALL=(ALL:ALL) NOPASSWD:ALL/' /etc/sudoers

echo 'https://git:YQXGs7jmZqss9oSD61_r@gitlab.com/ppatram/ansible_lab.git' > /root/.git-credentials
chmod 600 /root/.git-credentials

cd /home/generic
rm -rf ansible_lab

git clone https://git:YQXGs7jmZqss9oSD61_r@gitlab.com/ppatram/ansible_lab.git

cd ansible_lab

/bin/cp -f resources/generic_login /bin/generic_login
chmod 755 /bin/generic_login

# put the az key in generic's ssh
mkdir /home/generic/.ssh
/bin/cp -f resources/az_sshkeys.txt /home/generic/.ssh/id_rsa
chown -R generic. /home/generic/.ssh
chmod 700 /home/generic/.ssh
chmod 600 /home/generic/.ssh/id_rsa


# ssh setings
sed -i 's/^PasswordAuthentication .*$/PasswordAuthentication yes/' /etc/ssh/sshd_config
service sshd restart

# setup ansible
apt-add-repository -y ppa:ansible/ansible
apt-get -y update
apt-get -y install ansible python-pip
pip  install --upgrade pip  
