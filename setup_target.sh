#!/bin/bash -x
# setup the could vm for ansible

/bin/cp /root/.ssh/authorized_keys /tmp/
cat /tmp/id_ed25519.pub >> /tmp/authorized_keys
cat /tmp/authorized_keys | sort -u > /root/.ssh/authorized_keys
chmod 600 /root/.ssh/authorized_keys

# unmount the temporary disk
#sed  -Ei '/\s\/mnt\s/d' /etc/fstab
#umount /mnt
#dd if=/dev/zero of=/dev/sdb bs=50M count=1

# cleanup script
/bin/cp /tmp/cleanup.sh /root
chmod 755 /root/cleanup.sh

