#!/bin/bash

cd ~generic

if [ $1 == "" ]; then
  rm -rf logins login_times/ servers/ ssh_keys/
  
  for i in $(grep :auto_ /etc/passwd | cut -d: -f1); do
    killall -u $i -9
    userdel -r $i
  done
else
  echo "$@"
  for usr in $@; do
    uname=$(grep _${usr}: /etc/passwd | cut -d: -f1)
    if [ ! -z "$uname" ]; then
      rm -rf logins login_times/$usr servers/$usr ssh_keys/$usr
      killall -u $uname -9
      userdel -r $uname
    else
      echo "user $usr not present. Skipping!"
    fi
  done
fi  
