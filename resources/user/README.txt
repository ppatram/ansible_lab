hello y'all!

NOTE: You will need two sessions for this lab.
You are currently logged into one session. This session is where you will be running all of your playbooks.

Duplicate the steps you took to get this session using a differnt window or terminal from your laptop.
Then ssh as root to your target server:
ssh -oStrictHostKeyChecking=no root@__myserver__
	

##############  start of the exercises ##############################################################################
1. Do a generic ubuntu os config
Command:  ansible-playbook  -i inventories/hosts -vv sita-plain-ubuntu.yml

When you run the above command, it will generate an error as the server name in the inventory/hosts file is incorrect.
Currently, the server name in the inventory file is set to your username.
This is an intentional error generated on our part for safety purposes.

Please update the inventory/hosts file with the server name given in the error.


There are 6 variables which are required for this playbook to execute successfully.
They are the following:

    rootpw: joe
    vmtype: linux
    xsgrpv_do_vmsanitize: false
    xsgrpv_do_autofs_setup: false
    xsgrpv_do_satellite_reg: false
    xsgrpv_do_generic_packages: false

Add these variables one line at a time into the sita-plain-ubuntu.yml file and see what 
errors you get after each execution run.


2. Setup some users and groups on the system.
Command:  ansible-playbook  -i inventories/hosts -vv sita-function-add-user.yml
  a) create the default 'oracle' user and all its dependent groups
  b) create an an ad-hoc user

There is no right or wrong answer, just so long as you don't feed
variables which blow up the module.


3. Provision storage on the system
  a) Create a new filesysem
  b) Add a new disk to the volume group and resize the filesystem
  
Command: ansible-playbook  -i inventories/hosts -vv vmdk_storage.yml

If you make a mistake, login to your server as root:
ssh -oStrictHostKeyChecking=no root@__myserver__

then run the script:
./cleanup.sh


The playbooks are very flexible, and as a result they require very specific
configurations. See the "solutions" folder when you get stuck.


Extra credit:
What functionality do you think is missing? Can you implement it?
Please note that you *must* submit it to us. These machines will be destroyed.

To see this again, type:
cat ~/README.txt

Happy Ansiblizin'
